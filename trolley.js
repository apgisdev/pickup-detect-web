/*
	We need to clear the layer group every time, but the FIRST time through,
	the global marker & group have no value.  To get around this, we create
	a boolean to keep track if this is the first time the ping function is ran.
	If so, it will set the boolean to "false" at the end of the ping function.

*/
/*
	We need to clear the layer group every time, but the FIRST time through,
	the global marker & group have no value.  To get around this, we create
	a boolean to keep track if this is the first time the ping function is ran.
	If so, it will set the boolean to "false" at the end of the ping function.

*/

var redPrevLat = 0;
var redPrevLon = 0;
var redTime = "";
var greenPrevLat = 0;
var greenPrevLon = 0;
var greenTime = "";
var bluePrevLat = 0;
var bluePrevLon = 0;
var blueTime = "";

updateIcons();

// (function() {
//     setInterval(function() {
//         updateIcons();
//     }, 5000);
// });

setInterval(function () {
  updateIcons();
}, 5000);

var p_marker_group = new L.layerGroup([]);
var marker_open = false;
var last_clicked_marker;

// this happens every 5 seconds in index.html
function updateIcons() {

  //Array of trolley_ids
  var trolley_ids = [4, 5];
  //pass trolley_ids to the loadJSON function to add markers for that trolley
  loadJSON(4);

}

// called from updateIcons
function loadJSON(trolley_id) {
  var url = document.URL;
  var value = url.substr(url.indexOf("=") + 1);
  // The path to the desired data
  //  var data_file = "http://apnsgis3.apsu.edu/trolley/find?trolley_id="+trolley_id+"&sort=createdAt%20DESC&limit=10";
  // var data_file = "http://localhost:3000/trolley/find?trolley_id="+trolley_id+"&sort=createdAt%20DESC&limit=10";
  var data_file = "http://apnsgis3.apsu.edu/trolley/";

  var http_request = new XMLHttpRequest();
  try {
    // Opera 8.0+, Firefox, Chrome, Safari
    http_request = new XMLHttpRequest();
  } catch (e) {
    // Internet Explorer Browsers
    try {
      http_request = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        http_request = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e) {
        // Something went wrong
        alert("Your browser broke!");
        return false;
      }
    }
  }
  http_request.onreadystatechange = function () {

    if (http_request.readyState == 4) {
      //Clean-up the old markers
      p_marker_group.clearLayers();
      p_marker_group = new L.layerGroup([]);
      // Javascript function JSON.parse to parse JSON data
      //console.log(http_request.responseText);
      var coordinates_data = JSON.parse(http_request.responseText);

      var trolley2 = coordinates_data.trolley2;
      var trolley3 = coordinates_data.trolley3;
      var trolley4 = coordinates_data.trolley4;

      ping_location(trolley2.lat,
        trolley2.lon,
        trolley2.createdAt,
        trolley2.trolley_id,
        "red");

      ping_location(trolley3.lat,
        trolley3.lon,
        trolley3.createdAt,
        trolley3.trolley_id,
        "green");

      ping_location(trolley4.lat,
        trolley4.lon,
        trolley4.createdAt,
        trolley4.trolley_id,
        "blue");
    }
  }
  // XMLHttpRequest.open(method, url, ?async, ?user, ?password) // ? = optional
  http_request.open("GET", data_file, true);
  http_request.send();
}

function timeNow() {
  var d = new Date;
  var h = (d.getHours() < 10 ? '0' : '') + d.getHours();
  var m = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
  var s = (d.getSeconds() < 10 ? '0' : '') + d.getSeconds();

  return h + ':' + m + ':' + s;
}

function ping_location(lat, lon, createdAt, trolley_id, icon_color) {

  var trolley_marker = L.icon({ // http://leafletjs.com/reference-1.2.0.html#icon
    iconUrl: './images/trolley_' + icon_color + '.png',
    shadowUrl: './images/shadow.png',
    iconSize: [30, 45], // size of the icon
    iconAnchor: [15, 44],
    shadowAnchor: [18, 90], // point of the icon which will correspond to marker's location
    shadowSize: [60, 90],
    popupAnchor: [-2, -45] // point from which the popup should open relative to the iconAnchor
  });

  if (icon_color == "blue") {
    if (bluePrevLat != lat && bluePrevLon != lat) {
      blueTime = timeNow();
      bluePrevLat = lat;
      bluePrevLon = lon;
      //console.log("CHANGE BLUE");
    }
  }
  else if (icon_color == "green") {
    if (greenPrevLat != lat && greenPrevLon != lat) {
      greenTime = timeNow();
      greenPrevLat = lat;
      greenPrevLon = lon;
      //console.log("CHANGE GREEN");
    }
  }
  else{
    if (redPrevLat != lat && redPrevLon != lat) {
      redTime = timeNow();
      redPrevLat = lat;
      redPrevLon = lon;
      //console.log("CHANGE RED");
    }
  }

  var d = new Date(createdAt);
  var time = d.toLocaleTimeString("en-US", {
    timeZone: "America/Chicago"
  });
  var day = d.toLocaleDateString();

  //Define our marker, add it to the group, add it to the map, and open the pop-up
  var p_current_marker = new L.marker([lat, lon], {
    icon: trolley_marker
  }); // http://leafletjs.com/reference-1.2.0.html#marker
  //onclick listener to determine if the popup should stay up between reloads
  p_current_marker.on('click', function (e) {

    if (last_clicked_marker == trolley_id) {
      marker_open = !marker_open
    } else {
      marker_open = true;
    }

    last_clicked_marker = trolley_id;
  });

  //var popup = new L.popup().setContent(d.toString()); 

  var popup;
  if(icon_color == "blue")
    popup = new L.popup().setContent("<center>" + blueTime + "<br/>" + day + "</center>");
  else if (icon_color == "green")
    popup = new L.popup().setContent("<center>" + greenTime + "<br/>" + day + "</center>");
  else
    popup = new L.popup().setContent("<center>" + redTime + "<br/>" + day + "</center>");
  
  popup.options.autoPan = false;

  p_current_marker.bindPopup(popup);

  p_marker_group.addLayer(p_current_marker);
  p_marker_group.addTo(map);

  console.log('placed marker');

  if (marker_open && last_clicked_marker == trolley_id) {
    p_current_marker.openPopup();
  }
}