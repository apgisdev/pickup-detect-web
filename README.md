# PEAY PICKUP #

This web application is designed to show the active location of the three trolleys at APSU.

## Installing and Usage ##

Step 1: Clone this repository

Step 2: In Chrome go to this project's index.html

* In chrome place `file:///` in the URL bar.
* Click through folders until you get to the index.html of this project
  * ex: file:///Users/<user>/Desktop/pickup-detect/pickup-detect-web/index.html

## Details ##

The GPS updates about every 60 seconds, and this web page updates every 5 seconds.

## Work Flow ##

1. The index.html file calls updateIcons every 5 seconds
2. The updateIcons function is in the trolley.js file.
3. updateIcons calls loadJSON.
4. loadJSON makes a request to the server.
5. When ready the program takes the response JSON received and calls ping_location for each of the three trolleys.
6. ping_location sets the marker and shadow positions on the map.
7. After 5 seconds repeat from step 2.

### About ###

* Authors: Kenneth Hanley, Mason Cordell, Harrison Welch, Jake Crews
* Last Update: Thursday, September 14, 2017 1:10 PM
